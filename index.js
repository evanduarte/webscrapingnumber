const axios = require("axios");
const cheerio = require("cheerio");
const prompt = require('prompt-sync')();
 
const name = prompt('Nom de la société ? : ');



axios
  .get(`https://www.118000.fr/search?who=${name}`)
  .then((response) => {
    
    const html = response.data;

  
    const $ = cheerio.load(html);

  
    const scrapedata = $("a", ".card").text();

  
    //console.log(scrapedata);

    console.log("Numéro de la société {",name,"} :",scrapedata.match(/\d+/g).join('').slice(0,10))
  })

  .catch((error) => {
      if(error.response.status = 404) {
        console.error("Le Numéro de la société {",name,"} n'a pas été trouvé !") 
      }
      
  });


